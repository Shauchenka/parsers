<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/5/17
 * Time: 3:16 PM
 */

namespace Parser\Pleanala;


use GuzzleHttp\Client;
use Parser\PleanalaDb\WeekDataEntity;

class PleanalaParser
{

    /** @var  Client */
    private static $client;

    private static $insertId = 0;

    const BASE_URL = "http://www.pleanala.ie/";


    private static function getInsertId()
    {
        self::$insertId++;

        return self::$insertId;
    }

    /**
     * @return Client
     */
    private static function getCLient()
    {
        if(is_null(self::$client)){
            self::$client = new Client();
        }

        return self::$client;
    }

    /**
     * @param $url
     * @return \phpQueryObject|\QueryTemplatesParse|\QueryTemplatesSource|\QueryTemplatesSourceQuery
     */
    private static function getDocumentByUrl($url)
    {
        $response = self::getCLient()->request("get", $url, [
            'exceptions' => false
        ]);

        $html = $response->getBody()->getContents();

        return \phpQuery::newDocument($html);
    }

    /**
     * @param $yearUrl
     * @param $url
     * @return string
     */
    private static function convertYearYrl($yearUrl, $url)
    {
        $yearUrl = str_replace("../../", "/lists/", $yearUrl);
        $urlData = parse_url($url);

        return $urlData["scheme"] . "://" . $urlData["host"] . $yearUrl;
    }

    /**
     * @param $weekUrl
     * @param $url
     * @return string
     */
    private static function convertWeekUrl($weekUrl, $url)
    {
        $urlData = parse_url($url);

        return $urlData["scheme"] . "://" . $urlData["host"] . $weekUrl;
    }


    private static function beautyText($text)
    {
        return trim(preg_replace('/\s\s+/', ' ', $text));
    }

    /**
     * @param $url
     * @return array
     */
    public static function getYears($url)
    {
        $document = self::getDocumentByUrl($url);

        $resultArray = [];
        $yearlinks = $document->find(".mainContent p:first")->find("a");
        foreach ($yearlinks as $year){
            $year = pq($year);
            $resultArray[] = [
                "year" => $year->text(),
                "link" => self::convertYearYrl($year->attr("href"), $url)
            ];
        }
        
        return $resultArray;
    }


    /**
     * @param $year
     * @param \phpQueryObject $document
     * @return array
     */
    public static function parseYearPageByDocument($year, \phpQueryObject $document){
        $weeks = $document->find(".mainContent li")->find("a");
        $resultArray = [];
        foreach ($weeks as $week){
            $week = pq($week);
            $weekDate = $week->text();
            $weekDate = trim(str_replace("Week ending:", "", $weekDate));
            $weekLink = self::convertWeekUrl($week->attr("href"), BASE_URL);

            $dbId = self::getInsertId();

            $resultArray[$dbId] = [
                "year" => $year,
                "date" => $weekDate,
                "link" => $weekLink,
                "dbId" => $dbId
            ];
        }

        return $resultArray;
    }


    /**
     * @param $url
     * @return array
     */
    public static function parseYearPage($year, $url)
    {

        $document = self::getDocumentByUrl($url);
        return self::parseYearPageByDocument($year, $document);
    }

    public static function parseWeekPageDocument(\phpQueryObject $document, $weekId){
        $h3 = $document->find(".mainContent h3");
        $key = null;
        $resultArray = [];

        foreach ($h3 as $item){
            $item = pq($item);
            $hrefElement = $item->find("a");
            $hrefElement = pq($hrefElement);

            $link = $hrefElement->attr("href");
            $link = self::convertWeekUrl($link, BASE_URL);
            $name = $hrefElement->text();

            preg_match("/^\w+:/", $name, $idResult);

            $id = isset($idResult[0]) ? $idResult[0] : null;
            $id = trim(str_replace(":", "", $id));


            $h2 = $item->prev("h2")->text();
            if(is_null($key)){
                $key = $h2;
            }

            $key = $h2 == "" ? $key : $h2;

            $ULData = $item->next("ul")->find("li");

            $ulData = [];
            foreach ($ULData as $li){
                $li = pq($li);
                $liKey = $li->find("span:first")->text();
                $liText = $li->text();
                $liText = trim(str_replace($liKey . ":", "", $liText));

                $ulData[] = [
                    "key"  => $liKey,
                    "text" => $liText
                ];
            }

            $insertData = [
                "id"       => $id,
                "name"     => $name,
                "link"     => $link,
                "ulData"   => $ulData,
                "city"     => $key
            ];
            $weekData = self::insertWeekRecord($weekId, $insertData);
            $insertData["weekData"] = $weekData;
            $insertData["weekDataId"] = self::getInsertId();

            $resultArray[$insertData["weekDataId"]] = $insertData;

        }
        return $resultArray;
    }


    public static function parseWeekPage($url, $weekId = null)
    {
        $document = self::getDocumentByUrl($url);


    }

    public static function parseMoreInfoPageDocument(\phpQueryObject $document, $weekId){
        $main = $document->find(".mainContent");
        $main = pq($main);

        $name        = $main->find("p:first strong")->text();
        $description = $main->find("p:nth-child(2)")->text();

        $pageData = [
            "name"        => $name,
            "description" => $description
        ];

        $data = $main->find("p")->slice(2);

        foreach ($data as $oneP){
            $oneP = pq($oneP);
            $key = $oneP->find("strong")->text();
            $text = $oneP->text();

            $text = self::beautyText(str_replace($key, "", $text)); //remove key from text
            $key  = trim(str_replace(":", "", $key));

            $pageData[$key] = $text;
        }

        $otherData = $main->find("h2");


        foreach ($otherData as $h2){
            $h2 = pq($h2);
            $ulList = [];
            $h2Key = $h2->text();
            $allObjects = $h2->nextAll();
            foreach ($allObjects as $object){
                if($object->tagName == "ul"){
                    $ulList[] = pq($object);
                } else{
                    break;
                }
            }


            foreach ($ulList as $index => $ul){
                $ul = pq($ul);

                foreach ($ul->find("li") as $li){
                    if($h2Key == "Documents"){
                        $documentLink = pq($li)->find("a");
                        $fileLink = self::convertWeekUrl($documentLink->attr("href"), BASE_URL);

                        $linkParts = explode("/", $fileLink);
                        $fileName = end($linkParts);

                        $pageData[$h2Key][$index][] = [
                            "name"     => $documentLink->text(),
                            "fileName" => $fileName,
                            "link"     => $fileLink
                        ];

                    } else {
                        $pageData[$h2Key][$index][] = self::beautyText(pq($li)->text());
                    }
                }
            }
        }


        return $pageData;
    }

    public static function parseMoreInfoPage($url, $weekId)
    {
        $document = self::getDocumentByUrl($url);
        return self::parseMoreInfoPageDocument($document, $weekId);
    }

    public static function getFIle($url, $fileName)
    {
        $content = file_get_contents($url);
        file_put_contents(PDF_FILES_PATH . $fileName . ".pdf", $content);
    }


    private static function insertWeekRecord($weekId, $array)
    {
        $parsedData = self::getUlDataRecord($array["ulData"]);

        $entity = new WeekDataEntity();

        $entity->Site_Id = self::getValByKey("id" ,$array);
        $entity->Title = self::getValByKey("name" ,$array);
        $entity->Case_Type = self::getValByKey("Case Type", $parsedData);
        $entity->Description = self::getValByKey("Description", $parsedData);
        $entity->Decision = self::getValByKey("Decision", $parsedData);
        $entity->Planning_Authority = self::getValByKey("Planning Authority", $parsedData);
        $entity->Date_Lodged = self::getValByKey("Date lodged", $parsedData);
        $entity->Applicant = self::getValByKey("Applicant", $parsedData);
        $entity->EIS = self::getValByKey("EIS", $parsedData);
        $entity->NIS = self::getValByKey("NIS", $parsedData);
        $entity->Week_Id = $weekId;
        $entity->City = self::getValByKey("city", $array);

        return $entity->getDbData();
    }


    /**
     * @param $key
     * @param $array
     * @return null
     */
    private static function getValByKey($key, $array)
    {
        if(array_key_exists($key, $array)){
            return $array[$key];
        }

        return null;
    }

    private static function getUlDataRecord($array)
    {
        $result = [];
        foreach ($array as $key => $ulRecord){
            $result[$ulRecord["key"]] = $ulRecord["text"];
        }

        return $result;
    }
}