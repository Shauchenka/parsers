<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/25/17
 * Time: 1:06 PM
 */

namespace Parser\guzleParser;


interface DBSaverInterface
{
    public function setItemStorage($item, $dataBase);

}