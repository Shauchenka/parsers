<?php


namespace Parser\BetC2C;

use phpQuery;

class BetC2CParser
{

    /**
     * @return \Twig_Environment
     */
    private static function getTwig()
    {
        $loader = new \Twig_Loader_Filesystem('src/twig');
        $twig = new \Twig_Environment($loader);
        $twig->addExtension(new \Twig_Extension_Debug());
        return $twig;

    }

    /**
     * @param $html
     * @return array
     */
    public static function getMenu($html)
    {
        $document = phpQuery::newDocument($html);
        $inetWagerNumber = $document->find("#inetWagerNumber")->val();

        $sportTypes = $document->find(".sports-selection tr:not('.separator') td.sport-type");
        $resultArray = [];

        foreach ($sportTypes as $sportType) {
            $sportType = pq($sportType);
            $sportKey = $sportType->find(".sportHeader")->text(); //get Category name

            if (is_null($sportKey)) {
                continue; //skip category if name not found
            }

            $resultArray[$sportKey] = [
                "subelements" => [
                    "font" => [
                        "attributes" => [
                            "class" => "sportHeader"
                        ],
                        "text" => $sportKey
                    ]
                ],
                "data" => []
            ];

            $parentTable = $sportType->parents("table")->get(0);
            $parentTable = pq($parentTable);

            $categories = $parentTable->find("td:not('.sport-type')");

            foreach ($categories as $category) {
                $category = pq($category);
                $name = $category->find(".subSportHeaderLink")->text();

                if (is_null($name)) {
                    continue; //skip subcategory if name not found
                }

                $resultArray[$sportKey]["data"][$name] =  [
                    "attributes" => self::getSubMenuAttributes($category),
                    "subelements" => [
                        "font" => [
                            "attributes" => [
                                "class" => "subSportHeaderLink"
                            ],
                            "text"       => $name
                        ]
                    ]
                ];
            }
        }

        return [
            "inetWagerNumber" => $inetWagerNumber,
            "data"            => $resultArray
        ];

    }

    /**
     * @param \phpQueryObject $category
     * @return mixed
     */
    private static function getSubMenuAttributes(\phpQueryObject $category)
    {
        $link = $category->find("a");

        $attributes = [
            "id" => $link->attr("id"),
            "href" => $link->attr("href"),
            "language" => $link->attr("language"),
        ];

        return $attributes;
    }

    /**
     * @param $data
     */
    static function renderMenu($data)
    {
        echo self::getTwig()->render("BetC2CMenu.html.twig", [
            "categories" => $data
        ]);
    }

}