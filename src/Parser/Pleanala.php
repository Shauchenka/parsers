<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/25/17
 * Time: 1:05 PM
 */

namespace Parser\Pleanala;


use Parser\guzleParser\DBSaverInterface;
use Parser\guzleParser\guzleParser;

class Pleanala
{
    /** @var  DBSaverInterface */
    private $dbSaver;

    /**
     * Pleanala constructor.
     * @param DBSaverInterface $dbSaver
     */
    public function __construct(DBSaverInterface $dbSaver)
    {
        $this->dbSaver = $dbSaver;
    }


    public function getData($parseYears = [])
    {

        $totalTime = microtime(true);

        $request_guzzle = [];
        $cookiejar = new \GuzzleHttp\Cookie\CookieJar;

//get all year links
        $years = PleanalaParser::getYears(YEARS_URL);

        foreach ($years as $year) {
            if (!empty($parseYears) && !in_array($year["year"], $parseYears)) {
                continue;
            }
            $request_guzzle[] = [
                'form_params' => [],
                'method'      => 'GET',
                'index'       => $year["year"],
                'url'         => $year["link"],
                'cookies'     => $cookiejar,
            ];
        }
        $years = [];
        $start = microtime(true);
//send requests for each year page
        guzleParser::sendGuzzleRequest($request_guzzle, $years);
        $time = microtime(true) - $start;

        printf('Получение страниц с годами. Выполнялся %.4F сек.' . PHP_EOL, $time);

        $yearResult = [];

//parse each year page and merge results
        foreach ($years as $resp) {
            $output = $resp['output'];
            $index = $resp['index'];
            $document = \phpQuery::newDocument($output);
            $start = microtime(true);
            $oneResult = PleanalaParser::parseYearPageByDocument($index, $document);
            $yearResult += $oneResult;
            $time = microtime(true) - $start;
            printf('Парсинг страницы с годами и получение недель. Id: ' . $index . '. Выполнялся %.4F сек.' . PHP_EOL, $time);
        }


        $guzzleRequestParts = [];
        $index = 0;
        $iteration = 0;

//build requests for weeks
        foreach ($yearResult as $oneResult) {
            $guzzleRequestParts[$index][] = [
                'form_params' => [],
                'method'      => 'GET',
                'index'       => $oneResult["dbId"],
                'url'         => $oneResult["link"],
                'cookies'     => $cookiejar,
            ];

            $iteration++;

            if ($iteration >= COUNT_ASYNC_REQUESTS) {
                $iteration = 0;
                $index++;
            }
        }

        $totalResponse = [];

//send requests for weeks
        foreach ($guzzleRequestParts as $requestPart) {
            $response = [];
            printf("Колличество одновременно получаемых страниц = %d" . PHP_EOL, count($requestPart));
            $start = microtime(true);
            guzleParser::sendGuzzleRequest($requestPart, $response);
            $time = microtime(true) - $start;
            $totalResponse = array_merge($totalResponse, $response);
            printf('Получение страниц с неделями. выполнялся %.4F сек.' . PHP_EOL, $time);

        }

        $result = [];
        $count = 0;

        $weekIdTree = [];

//parse week pages
        foreach ($totalResponse as $resp) {
            $output = $resp['output'];
            $index = $resp['index'];
            $document = \phpQuery::newDocument($output);
            $start = microtime(true);
            $yearResult[$index]["weeks"] = PleanalaParser::parseWeekPageDocument($document, $index);
            $result += $yearResult[$index]["weeks"];
            $arrayKeys = array_keys($yearResult[$index]["weeks"]);
            foreach ($arrayKeys as $key) {
                $weekIdTree[$key] = $index;
            }
            $time = microtime(true) - $start;
            printf('Парсинг страниц по неделе. Id:' . $index . '. Выполнялся %.4F сек.' . PHP_EOL, $time);
            $count++;
        }

        $guzzleRequestParts = [];
        $index = 0;
        $iteration = 0;


//build requests for pages in week
        foreach ($result as $one) {
            $guzzleRequestParts[$index][] = [
                'form_params' => [],
                'method'      => 'GET',
                'index'       => $one["weekDataId"],
                'url'         => $one["link"],
                'cookies'     => $cookiejar,
            ];

            $iteration++;

            if ($iteration >= COUNT_ASYNC_REQUESTS) {
                $iteration = 0;
                $index++;
            }
        }

        $totalResponse = [];

//send requests for pages in week
        foreach ($guzzleRequestParts as $requestPart) {
            $response = [];
            printf("Колличество одновременно получаемых страниц = %d" . PHP_EOL, count($requestPart));
            $start = microtime(true);
            guzleParser::sendGuzzleRequest($requestPart, $response);
            $time = microtime(true) - $start;
            $totalResponse = array_merge($totalResponse, $response);
            printf('Получение дополнительной информации из записей в неделях. %.4F сек.' . PHP_EOL, $time);

        }

        $arrayFiles = [];

//parse more info in page
        foreach ($totalResponse as $resp) {
            $output = $resp['output'];
            $index = $resp['index'];
            $document = \phpQuery::newDocument($output);
            $start = microtime(true);
            $weekData = $yearResult[$weekIdTree[$index]]["weeks"][$index]["weekData"];
            $pageData = PleanalaParser::parseMoreInfoPageDocument($document, $index);

            if(isset($pageData["Date Signed"]) && $dateSigned = \DateTime::createFromFormat('!d/m/Y', $pageData["Date Signed"])){
                $dateSigned = $dateSigned->getTimestamp();
            } else {
                $dateSigned = $yearResult[$weekIdTree[$index]]["date"];
            }

            $weekData["Name"] = $pageData["name"];
            $weekData["Description_P"] = isset($pageData["description"]) ? $pageData["description"] : "";
            $weekData["Case_Reference"] = isset($pageData["Case reference"]) ? $pageData["Case reference"] : "";
            $weekData["Date_Signed"] = isset($pageData["Date Signed"]) ? $pageData["Date Signed"] : "";
            $weekData["time"] = $dateSigned;

            $parties = [];
            $history = [];
            $documents = [];

            if (isset($pageData["Parties"])) {
                foreach ($pageData["Parties"] as $one) {
                    foreach ($one as $data) {
                        $parties[] = $data;
                    }
                }
            }

            if (isset($pageData["History"])) {
                foreach ($pageData["History"] as $one) {
                    foreach ($one as $data) {
                        $history[] = $data;
                    }
                }
            }
            if (isset($pageData["Documents"])) {
                foreach ($pageData["Documents"] as $one) {
                    foreach ($one as $data) {
                        $documents[] = $data;
                    }
                }
            }

            $history = array_unique($history);
            $weekData["Parties"] = $parties;
            $weekData["History"] = $history;
            $weekData["Documents"] = $documents;

            $weekData["weekDataId"] = $yearResult[$weekIdTree[$index]]["weeks"][$index]["weekDataId"];
            $this->dbSaver->setItemStorage($weekData, DATABASE);
//    $yearResult[$weekIdTree[$index]]["weeks"][$index] = $weekData;
            if (array_key_exists("Documents", $pageData)) {
                $arrayFiles = array_merge($arrayFiles, $pageData["Documents"][0]);
            }
            $time = microtime(true) - $start;
            printf('Парсинг домолнительной информации. Id:' . $index . '. Выполнялся %.4F сек.' . PHP_EOL, $time);
            $count++;
        }


        $guzzleRequestParts = [];
        $index = 0;
        $iteration = 0;
//create requests for files
        foreach ($arrayFiles as $file) {
            if (!file_exists(PDF_FILES_PATH . $file["fileName"])) {
                $guzzleRequestParts[$index][] = [
                    'form_params' => [],
                    'method'      => 'GET',
                    'index'       => $file["fileName"],
                    'url'         => $file["link"],
                    'cookies'     => $cookiejar,
                ];

                $iteration++;

                if ($iteration >= COUNT_ASYNC_REQUESTS) {
                    $iteration = 0;
                    $index++;
                }
            }
        }

//save files
        foreach ($guzzleRequestParts as $requestPart) {
            $response = [];
            printf("Колличество одновременно получаемых страниц = %d" . PHP_EOL, count($requestPart));
            $start = microtime(true);
            guzleParser::sendGuzzleRequest($requestPart, $response);
            foreach ($response as $resp) {
                $output = $resp['output'];
                $index = $resp['index'];
                if(!file_exists(PDF_FILES_PATH . $index)){
                    file_put_contents(PDF_FILES_PATH . $index, $output);
                }
            }
            $time = microtime(true) - $start;
            printf('Получение файлов. %.4F сек.' . PHP_EOL, $time);

        }

        $time = microtime(true) - $totalTime;
        printf('Общее время выполнения %.4F сек.' . PHP_EOL, $time);
    }
}