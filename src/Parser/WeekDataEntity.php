<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/9/17
 * Time: 1:19 PM
 */

namespace Parser\PleanalaDb;


class WeekDataEntity
{
    private $dbFields = [
       "Site_Id"             => null,
       "Title"               => null,
       "Case_Type"           => null,
       "Description"         => null,
       "Decision"            => null,
       "Planning_Authority"  => null,
       "Date_Lodged"         => null,
       "Applicant"           => null,
       "EIS"                 => null,
       "NIS"                 => null,
       "Name"                => null,
       "Description_P"       => null,
       "Case_Reference"      => null,
       "Date_Signed"         => null,
       "Week_Id"             => null,
       "City"                => null,
    ];

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if(array_key_exists($name, $this->dbFields)){
            $this->dbFields[$name] = $value;
        }
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if(array_key_exists($name, $this->dbFields)){
            return $this->dbFields[$name];
        }
        return null;
    }

    /**
     * @return array
     */
    public function getDbValues()
    {
        return array_values($this->dbFields);
    }

    /**
     * @return array
     */
    public function getDbKeys()
    {
        return array_keys($this->dbFields);
    }

    /**
     * @return array
     */
    public function getDbData()
    {
        return $this->dbFields;
    }

}