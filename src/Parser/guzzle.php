<?php 
namespace Parser\guzleParser;


use GuzzleHttp\Client;
use function GuzzleHttp\Promise\settle;
use function GuzzleHttp\Promise\unwrap;

class guzleParser
{
    private static $guzzle  	=   null;

    public static function initGuzzle()
    {
        if (self::$guzzle == null) {
            self::$guzzle = new Client();
            return self::$guzzle;
        } else {
            return self::$guzzle;
        }
    }

    public static function sendGuzzleRequest($requests, &$result)
    {
        $client = self::initGuzzle();
        $promises = [];
        foreach ($requests as $request) {
            if ($request['method'] == "POST") {
                $data = [
                    'force_ip_resolve' => 'v4',
                    'form_params' => $request['form_params'],
                    'allow_redirects' => false,
                    'cookies' => $request['cookies']
                ];
                if (isset($request['headers'])) if (is_array($request['headers'])) {
                    $data['headers'] = $request['headers'];
                };
                try{
                    $promises[$request['index']] = $client->postAsync($request['url'], $data);
                } catch (\Exception $e){
                    echo "ERROR!!! " . $e->getMessage() . ". URL: " . $request["url"] . PHP_EOL;
                    $promises[$request['index']] = "";
                }

            } elseif ($request['method'] = "GET") {
				 $data = [
                    'force_ip_resolve' => 'v4',
                    'allow_redirects' => false,
                    'cookies' => $request['cookies']
                ];
                if (isset($request['headers'])) if (is_array($request['headers'])) {
                    $data['headers'] = $request['headers'];
                };
                try{
                    $promises[$request['index']] = $client->getAsync($request['url'], $data);
                } catch (\Exception $e){
                    echo "ERROR!!! " . $e->getMessage() . ". URL: " . $request["url"] . PHP_EOL;
                    $promises[$request['index']] = "";
                }
            }
        }

        $results = unwrap($promises);
        $results = settle($promises)->wait();
        foreach ($results as $index => $promise) {
            $body = $results[$index]['value']->getBody();
            $result[] = [
                'output' => $body->getContents(),
                'index' => $index
            ];
        }
        return true;
    }
}
