<?php
/**
 * Created by PhpStorm.
 * User: tshauchenka
 * Date: 10/2/17
 * Time: 3:43 PM
 */

namespace AVsite;

use AVsite\Exception\PageNotFoundException;
use GuzzleHttp\Client;
use phpQuery;

class AVParser
{

    /** @var  Client */
    private $clinet;

    function __construct()
    {
        $this->clinet = new Client();
    }

    /**
     * @return Client
     */
    public function getClinet()
    {
        return $this->clinet;
    }

    public function getAllCarIds($baseUrl)
    {
        $page = 1;
        $carIds = [];
        while(true){
            $url = $baseUrl . "/" . $page;
            try{
                $pageIds = $this->getCarIdsByPage($url);
            } catch (PageNotFoundException $exception){
                break;
            }

            $carIds = array_merge($carIds, $pageIds);

            $page++;
        }

        return array_unique($carIds);
    }

    /**
     * @param $url
     * @return array
     * @throws PageNotFoundException
     */
    public function getCarIdsByPage($url)
    {
        $response = $this->clinet->request("GET", $url, [
            'exceptions' => false
        ]);

        if($response->getStatusCode() <= 300){
            $html = $response->getBody()->getContents();
            $document = phpQuery::newDocument($html);
            return $this->parseIds($document);
        } else {
            throw new PageNotFoundException();
        }
    }

    /**
     * @param \phpQueryObject $document
     * @return array
     */
    private function parseIds(\phpQueryObject $document)

    {
        $cars = $document->find(".listing-item");
        $carIds = [];

        foreach ($cars as $car){
            $carObj = pq($car);
            $bookmark = $carObj->find(".bookmark");
            $bookmark = pq($bookmark);
            $carIds[] = $bookmark->attr("data-id-ids");
        }

        return $carIds;
    }

}