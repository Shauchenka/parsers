<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);
set_time_limit(0);
//ignore_user_abort(true);
ini_set('memory_limit', '4096M');
header('X-Accel-Buffering: no');
ini_set('output_buffering', 'Off');
ini_set('output_handler', '');
ini_set('zlib.output_handler','');
ini_set('zlib.output_compression', 'Off');
# ini_set('implicit_flush', 'On');
while (ob_get_level()) { ob_end_flush(); }
ob_implicit_flush(true);
header('Content-Type: text/html; charset=UTF-8');
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_regex_encoding('UTF-8');

define("DATABASE", "test");
define("PDF_FILES_PATH", "docs/");
define("BASE_URL", "http://www.pleanala.ie/");
define("YEARS_URL", "http://www.pleanala.ie/lists/2016/decided/index.htm");
define("COUNT_ASYNC_REQUESTS", 100);
