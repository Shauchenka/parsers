<?php

use Parser\guzleParser\DbSaver;
use Parser\Pleanala\Pleanala;

require_once 'vendor/autoload.php';
require 'src/Parser/DBSaverInterface.php';
require 'config.php';
require 'src/Parser/Pleanala.php';
require 'src/Parser/DbSaver.php';
require 'src/Parser/PleanalaParser.php';
require 'src/Parser/WeekDataEntity.php';
require 'src/Parser/guzzle.php';


$dbSaver = new DbSaver();

$pleanala = new Pleanala($dbSaver);

$pleanala->getData();//может принимать аргументом массив лет, которые надо парсить. Если не передать то парсит все года